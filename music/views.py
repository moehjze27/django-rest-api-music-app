# Create your views here.
from .models import Artist, Song, Album
from rest_framework import viewsets
from .serializers import ArtistSerializer, SongSerializer, AlbumSerializer

class SongViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows groups to be viewed or edited.
    """
    queryset = Song.objects.all()
    serializer_class = SongSerializer

class AlbumViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows groups to be viewed or edited.
    """
    queryset = Album.objects.all().order_by('-birth_date')
    serializer_class = AlbumSerializer

class ArstistViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows users to be viewed or edited.
    """
    queryset = Artist.objects.all().order_by('-name')
    serializer_class = ArtistSerializer

