from django.db import models
import datetime
from django.utils import timezone

# Create your models here.
class Artist(models.Model):
    name = models.CharField(max_length=20, default=None)
    image = models.ImageField(upload_to='media/ArtistImages/', default=None)
    desc = models.TextField(max_length=500, blank = True, default=None)

class Album(models.Model):
    title = models.CharField(max_length=20, default=None)
    artist = models.ForeignKey(Artist, on_delete=models.CASCADE, related_name="+")
    image = models.ImageField(upload_to='media/AlbumImages/')
    birth_date = models.DateTimeField('date creation')
    desc = models.TextField(max_length=500, blank = True)


class Song(models.Model):
    name = models.CharField(max_length=20, default=None)
    song = models.FileField(upload_to='media/music/')
    album = models.ForeignKey(Album, on_delete=models.CASCADE, related_name="+", default=None)
    artist = models.ForeignKey(Artist, on_delete=models.CASCADE, related_name="+")




    