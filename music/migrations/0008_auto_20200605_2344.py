# Generated by Django 3.0.7 on 2020-06-05 23:44

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('music', '0007_auto_20200605_2339'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='artist',
            name='album',
        ),
        migrations.RemoveField(
            model_name='artist',
            name='song',
        ),
        migrations.AlterField(
            model_name='album',
            name='artist',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='+', to='music.Artist'),
        ),
    ]
